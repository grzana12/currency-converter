<?php

namespace Application\Service;

class Currency
{

	/**
	 * @see https://free.currencyconverterapi.com
	 * Free currency converter API. Request limited to 100 per hour
	 */
	const URL = 'https://free.currencyconverterapi.com/api/v6/convert?q={QUERY}&compact=y';

	/**
	 * @var string|null
	 */
	private $_from = null;

	/**
	 * @var string|null
	 */
	private $_to = null;

	/**
	 * @param string $currency
	 *
	 * @return $this
	 */
	public function setFrom($currency)
	{
		$this->_from = $currency;
		return $this;
	}

	/**
	 * @param string $currency
	 *
	 * @return $this
	 */
	public function setTo($currency)
	{
		$this->_to = $currency;
		return $this;
	}

	/**
	 * Convert value to requested currency defined in "$_to" value
	 *
	 * @param int $amount
	 *
	 * @return string
	 */
	public function convert($amount)
	{
		$from_Currency = urlencode($this->_from);
		$to_Currency = urlencode($this->_to);
		$query =  "{$from_Currency}_{$to_Currency}";

		$url = str_replace('{QUERY}', $query, self::URL);
		$json = file_get_contents($url);
		$obj = json_decode($json, true);

		$val = floatval($obj["$query"]['val']);

		$total = $val * $amount;
		return number_format($total, 2, '.', '');
	}

}