<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\Convert as ConvertForm;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
    	/** Create default form */
    	$convertForm = new ConvertForm;
		$convertForm->prepare();

        return new ViewModel([
        	'form' => $convertForm
		]);
    }
}
