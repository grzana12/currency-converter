<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Application\Service\Currency as CurrencyService;
use Application\Service\Currency;
use Application\Validate\Currency as CurrencyValidator;

class ConvertController extends AbstractRestfulController
{
    public function indexAction()
    {
    	/** @var int $value Currency value */
    	$value = $this->params()->fromQuery('value', 0);

    	$validator = new CurrencyValidator;

    	if($validator->isValid($value))
		{
			/** @var CurrencyService $service */
			$service = new CurrencyService();
			$service
				->setFrom('RUB')
				->setTo('PLN');

			$result = [
				'result' => 'ok',
				'from' => $value,
				'to' => $service->convert($value)
			];
		}
		else
		{
			$result = [
				'result' => 'error',
				'messages' => $validator->getMessage()
			];
		}

		return new JsonModel($result);
	}
}
