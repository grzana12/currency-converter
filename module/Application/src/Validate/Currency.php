<?php

namespace Application\Validate;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Validator;

class Currency
{

	/**
	 * @var InputFilter
	 */
	private $_filter;

	public function __construct()
	{

		$value = new Input('currency');
		$value->getValidatorChain()
			->attach(new Validator\NotEmpty())
			->attach(new Validator\Digits())
		;

		$this->_filter = new InputFilter();
		$this->_filter->add($value);

	}

	/**
	 * @param mixed $value
	 *
	 * @return bool
	 */
	public function isValid($value)
	{
		$this->_filter->setData(['currency' => $value]);
		return $this->_filter->isValid();
	}

	/**
	 * @return null|string[]
	 */
	public function getMessage()
	{
		$message = null;

		foreach($this->_filter->getInvalidInput() as $error)
		{
			$message = $error->getMessages();
		}

		return $message;
	}

}