<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element\Number;
use Zend\Form\Element\Submit;

class Convert extends Form
{

	public function __construct( $name = null, array $options = [] )
	{
		parent::__construct( $name, $options );

		$this->init();
	}

	public function init()
	{
		/** Confugure form */
		$this->setAttribute('method', 'POST');
		$this->setAttribute('class', 'form-inline convert-form');

		/** Create a number element to capture the currency value */
		$from = new Number('currency_from');
		$from->setLabel('RUB:');
		$from->setAttribute('class', 'form-control');
		$from->setAttribute('placeholder', 'Amount');

		/** Create a submit button */
		$send = new Submit('convert');
		$send->setValue('Convert');
		$send->setAttribute('class', 'btn btn-primary');

		/** Add elements to form */
		$this->add($from);
		$this->add($send);

	}

}