$(function() {

	$('.convert-form').on('submit', function(e){
		e.preventDefault();


		$('.result-box .message-box').hide();
		$('.result-box table').hide();

		var url = $(this).attr('action');
		var currencyInput = $('input[name="currency_from"]');
		var currencyBtn = $('.btn');

		var value = currencyInput.val();

		currencyInput.attr('disabled', 'disabled');
		currencyBtn.attr('disabled', 'disabled');

		$('.currency').text('');

		$.get(url, {'value': value}, function(result){


			currencyInput.removeAttr('disabled');
			currencyBtn.removeAttr('disabled');

			if(result['result'] == 'ok')
			{
				$('.result-box table').show();

				$('.currency_from').text( result['from'] );
				$('.currency_to').text( result['to'] );
			}
			else
			{
				var box = $('.result-box .message-box');

				box.empty();

				$.each(result['messages'], function(row, message){
					box.append('<li>'+ message +'</li>');

				});

				box.show();

			}


		});

	});
});